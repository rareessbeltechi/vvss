package biblioteca.repository.repoMock;

import biblioteca.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class CartiRepoMockTest {
    CartiRepoMock biblioteca;

    @Before
    public void setUp() throws Exception {
        biblioteca = new CartiRepoMock();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void cautaCarte1() throws Exception {
        String ref = "Eminescu";
        List<Carte> cartiRezultat = biblioteca.cautaCarte(ref);
        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaCarte2() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Eminescu"));
        c.setAnAparitie("1992");
        biblioteca.adaugaCarte(c);

        String ref = null;

        List<Carte> cartiRezultat = biblioteca.cautaCarte(ref);
        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaCarte3() throws Exception {
        Carte c = new Carte();
        c.setTitlu("CarteDoi");
        c.setReferenti(new ArrayList<String>());
        c.setAnAparitie("1992");
        biblioteca.adaugaCarte(c);
        String ref = "Eminescu";

        List<Carte> cartiRezultat = biblioteca.cautaCarte(ref);

        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaCarte4() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Labis"));
        c.setAnAparitie("1992");
        biblioteca.adaugaCarte(c);

        Carte c1 = new Carte();
        c1.setTitlu("CarteDoi");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1992");
        biblioteca.adaugaCarte(c1);

        String ref = "Eminescu";

        List<Carte> cartiRezultat = biblioteca.cautaCarte(ref);

        assertTrue(cartiRezultat.isEmpty());
    }

    @Test
    public void cautaCarte5() throws Exception {
        Carte c = new Carte();
        c.setTitlu("Carte");
        c.setReferenti(asList("Labis"));
        c.setAnAparitie("1992");
        biblioteca.adaugaCarte(c);

        Carte c1 = new Carte();
        c1.setTitlu("CarteDoi");
        c1.setReferenti(asList("Blaga"));
        c1.setAnAparitie("1992");
        biblioteca.adaugaCarte(c1);

        Carte c2 = new Carte();
        c2.setTitlu("CarteTrei");
        c2.setReferenti(asList("Eminescu"));
        c2.setAnAparitie("1992");
        biblioteca.adaugaCarte(c2);

        String ref = "Eminescu";

        List<Carte> cartiRezultat = biblioteca.cautaCarte(ref);

        assertEquals(cartiRezultat.size(), 1);
    }
}