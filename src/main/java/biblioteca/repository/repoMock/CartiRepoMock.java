package biblioteca.repository.repoMock;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepoMock implements CartiRepoInterface {

    private List<Carte> carti;

    public CartiRepoMock() {
        carti = new ArrayList<Carte>();

//        carti.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
//        carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
//        carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
//        carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
//        carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
//        carti.add(Carte.getCarteFromString("Test;Calinescu,Tetica;1992;Pipa;am,casa"));

    }

    @Override
    public void adaugaCarte(Carte c) {

        carti.add(c);
    }

    @Override
    public List<Carte> cautaCarte(String ref) {
        List<Carte> carti = getCarti();//1
        List<Carte> cartiGasite = new ArrayList<Carte>();//2
        if (ref == null) {//3
            return cartiGasite;//4
        }
        int i = 0;//5
        while (i < carti.size()) {//6
            List<String> autori = carti.get(i).getAutori();//7
            if (!autori.isEmpty()) {//8
                boolean flag = hasAuthor(ref, carti.get(i).getAutori());//9
                if (flag) {//10
                    cartiGasite.add(carti.get(i));//1
                }
            }
            i++;//12
        }
        return cartiGasite;//13
    }

    private boolean hasAuthor(String ref, List<String> autori) {
        int j = 0;
        boolean flag = false;
        while(j<autori.size()){
            if(autori.get(j).toLowerCase().contains(ref.toLowerCase())){
                flag = true;
                break;
            }
            j++;
        }
        return flag;
    }

    @Override
    public List<Carte> getCarti() {
        return carti;
    }

    @Override
    public void modificaCarte(Carte nou, Carte vechi) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stergeCarte(Carte c) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Carte> getCartiOrdonateDinAnul(String an) {
        List<Carte> lc = getCarti();
        List<Carte> lca = new ArrayList<Carte>();
        for (Carte c : lc) {
            if (c.getAnAparitie().equals(an)) {
                lca.add(c);
            }
        }

        Collections.sort(lca, new Comparator<Carte>() {

            @Override
            public int compare(Carte a, Carte b) {
                if (a.getTitlu().compareTo(b.getTitlu()) == 0) {
                    return a.getAutori().get(0).compareTo(b.getAutori().get(0));
                }

                return a.getTitlu().compareTo(b.getTitlu());
            }

        });

        return lca;
    }

}
