package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    BibliotecaCtrl bibliotecaCtrl;
    CartiRepoMock cartiRepoMock;
    Carte c1, c2;
    Carte c3, c4;
    Carte c5, c6;
    Carte c7, c8;
    Carte c9, c10;
    Carte c11, c12;
    Carte c13;
    @Before
    public void setUp() throws Exception {

        cartiRepoMock = new CartiRepoMock();
        bibliotecaCtrl = new BibliotecaCtrl(cartiRepoMock);

        c1 = new Carte("Povestiri", new ArrayList<String>(Arrays.asList("Eminescu", "Creanga")), "1973", new ArrayList<String>(Arrays.asList("Corint")));
        c2 = new Carte("Poeziiciuni", new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));

        c3 = new Carte("Povestiri", new ArrayList<String>(Arrays.asList("Eminescu", "Creanga")), "19713", new ArrayList<String>(Arrays.asList("Corint")));
        c4 = new Carte("Poeziiciuni", new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));

        c5 = new Carte("Povestiri", new ArrayList<String>(Arrays.asList("Eminescu", "Creanga")), "1973", new ArrayList<String>(Arrays.asList("Corint")));
        c6 = new Carte("Poeziiciuni", new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));
        c7 = new Carte("PovestiriasdfasdfPovestiriasdfasdf", new ArrayList<String>(Arrays.asList("Eminescu", "Creanga")), "1971", new ArrayList<String>(Arrays.asList("Corint")));
        c8 = new Carte("Poeziiciuni", new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));



        c9 = new Carte("Povestiri", new ArrayList<String>(Arrays.asList("Eminescu", "Creanga")), "1973", new ArrayList<String>(Arrays.asList("Corint")));
        c10 = new Carte("Poeziiciuni", new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));
        c11= new Carte("Povestiri", null, "1971", new ArrayList<String>(Arrays.asList("Corint")));
        c12 = new Carte("Poeziiciuni", new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));

        c13 = new Carte(null, new ArrayList<String>(Arrays.asList("Sadoveanu")), "1972", new ArrayList<String>(Arrays.asList("Corint", "poezii")));

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void ECP_VALID1() {
        try {
            bibliotecaCtrl.adaugaCarte(c1);
            bibliotecaCtrl.adaugaCarte(c2);

        } catch (Exception e) {
            fail("O picat ECP-Valid1");
            System.out.println(e.toString());
        }

    }

    @Test
    public void ECP_Invalid1() {
        try {
            bibliotecaCtrl.adaugaCarte(c3);
            bibliotecaCtrl.adaugaCarte(c4);
            fail("O picat ECP-INVALID1");

        } catch (Exception e) {
            assertTrue(true);
            System.out.println(e.toString());
        }
    }


    @Test
    public void ECP_VALID2() {
        try {
            bibliotecaCtrl.adaugaCarte(c5);
            bibliotecaCtrl.adaugaCarte(c6);
            assertTrue(true);
        } catch (Exception e) {
            fail("O picat ECP-Valid2");
            System.out.println(e.toString());
        }

    }

    @Test
    public void ECP_Invalid2() {
        try {
            bibliotecaCtrl.adaugaCarte(c7);
            bibliotecaCtrl.adaugaCarte(c8);
            fail("O picat ECP-INVALID2");
        } catch (Exception e) {
            assertTrue(true);
            System.out.println(e.toString());
        }
    }

    @Test
    public void ECP_VALID3() {
        try {
            bibliotecaCtrl.adaugaCarte(c9);
            bibliotecaCtrl.adaugaCarte(c10);
            assertTrue(true);
        } catch (Exception e) {
            fail("O picat ECP-Valid3");
            System.out.println(e.toString());
        }

    }

    @Test
    public void ECP_Invalid3() {
        try {
            bibliotecaCtrl.adaugaCarte(c11);
            bibliotecaCtrl.adaugaCarte(c12);
            fail("O picat ECP-INVALID3");
        } catch (Exception e) {
            assertTrue(true);
            System.out.println(e.toString());
        }
    }


    @Test
    public void BVA(){

        assertEquals(c13.getTitlu(),null);

        assertEquals(c12.getTitlu(),"Poeziiciuni");
        assertEquals(c12.getTitlu().length(),11);

        assertEquals(c11.getTitlu(),"Povestiri");
        assertEquals(c11.getTitlu().length(),9);

    }

    @Test
    public void BVA1(){
        assertTrue(Integer.parseInt(c11.getAnAparitie())>1900);
        assertTrue(Integer.parseInt(c12.getAnAparitie())>1900);
        assertTrue(Integer.parseInt(c13.getAnAparitie())>1900);
        assertTrue(Integer.parseInt(c9.getAnAparitie())>1900);
    }
}